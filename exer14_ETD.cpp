/* 
 * File:   exer14_ETD.cpp
 * Author: Devs
 *
 * Created on 6 de Maio de 2018, 04:11
 * 
 * 14. Faça um programa que leia um vetor de 10 posições e realize a ordenação pelo algoritmo quicksort.
 */

#include <iostream>

using namespace std;

const int TAM = 10;
int vetor [TAM];

void quickSort(int vetor[], int TAM, int inicio, int fim){
    int esquerda, pivo, meio, direita;
    
    esquerda  = inicio;
    direita = fim;
    meio = (int) ((esquerda + direita) /2);
    pivo = vetor[meio];
    while( direita > esquerda){
        while(vetor[esquerda] < pivo){
            esquerda = esquerda + 1;
        }
        if (esquerda <= direita){
            int aux = vetor[esquerda];
            vetor[esquerda] = vetor[direita];
            vetor[direita] = aux;
        
            esquerda = esquerda + 1;
            direita = direita - 1;
        }
        
    }
    
}
int main(void) {

    return 0;
}


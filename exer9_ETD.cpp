/*
 * 9. Desenvolva um programa que ordene um vetor de inteiro com dez posições. Os valores devem ser
 *lidos no terminal. Usar o bublle sort.
 */

/* 
 * File:   exer9_ETD.cpp
 * Author: Devs
 *
 * Created on 5 de Maio de 2018, 20:27
 */

#include <iostream>

using namespace std;
const int TAM = 10;
int vetor[TAM];

void lerVetor(int vetor[], int TAM){
    cout<<"PREENCHA O VETOR"<<endl;
    for(int i = 0; i < TAM; i++){
        cout << "Vator ["<<(i)<<"]"<<": ";
        cin>>vetor[i];
    }
}

void bublleSort(int vetor[], int TAM){
    for( int x = 0; x < TAM; x++){       
        for(int y = x+1; y < TAM; y++){
            if(vetor[x] > vetor[y]){
                int maior = vetor[x];
                vetor[x] = vetor[y];
                vetor[y] = maior;
            }
        }
    }
}
void imprimirVetor(int vetor[], int TAM){
    cout<<endl;
    cout<<"VETOR ORDENADO (Bublle Sort"<<endl;
    for(int i = 0; i < TAM; i++){
           cout << "Posicao["<<(i)<<"]" <<": "<< vetor[i]<<endl;
    }    
}
int main(void) {
    
    lerVetor(vetor, TAM);
    bublleSort(vetor, TAM);
    imprimirVetor(vetor, TAM);
    
    return 0;
}


/* 
 * File:   exer8_ETD.cpp
 * Author: mATHEUS 
 *
 * Created on 5 de Maio de 2018, 13:52
 */

/*
 *  8. Implemente o algoritmo de ordenação selection sort e aplique a um vetor com vinte posições. Os
    valores do vetor devem ser lidos pelo usuário e apresentados, ordenados, na tela do programa.
 */
#include <iostream>

using namespace std;
const int TAN = 20;
int vetor[TAN];
void lerVetor(int vetor[], int TAN){
    cout<<"Adicinar valores ao Vetor"<<endl;
    for(int i = 0; i < TAN; i++){
        cout<< "Posicao ["<< (i)<< "]: ";
        cin>>vetor[i];
    }
    cout<<endl;
}
void imprimir(int vetor[], int TAN){
    cout<<"VETOR ORDENADO"<<endl;
    for(int i = 0; i < TAN; i++){
        cout<< "Posicao ["<< (i)<< "]: " <<(vetor[i])<<endl;
    }
}
 
void ordenacao(int vetor[], int TAN){
        for (int i=0; i < TAN; i++){
            int menor = i;
            for (int j=i+1; j < TAN; j++){
                if (vetor[j] < vetor[menor]){
                    menor = j;
                }
            }
        
        if (i != menor){
            int temp = vetor[i];
            vetor[i] = vetor[menor];
            vetor[menor] = temp;
        }
    }    
    cout<<endl;
}

int main(void) {
    lerVetor(vetor, TAN);
    ordenacao(vetor, TAN);
    imprimir(vetor, TAN);
    return 0;
}
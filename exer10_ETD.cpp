/*
 * File:   exer10_ETD.cpp
 * Author: Devs
 *
 * Created on 5 de Maio de 2018, 21:08
 * 10. Faça um programa que preencha dez posições de um vetor da estrutura a seguir:
 *struct pontos {
 int x;
 int y;
};
 * E realize as seguintes operações:
a) Apresente o maior valor de x entre os "pontos"(vetor);
b) Apresente o menor valor de y entre os "pontos"(vetor);
c) Dado um valor, imprimir em qual posição se encontra este valor. Se o valor estiver em várias
posições, apresentar todas as posições em que encontra determinado valor.
 */

#include <iostream>

using namespace std;
const int TAM = 10;
struct pontos{
    int x;
    int y;
};
struct pontos vetor[TAM];
void preenchaVetor(struct pontos vetor[], int TAM){
   cout<< "PREENCHA O VETOR DE 10 POSICOES"<<endl;
   //struct pontos vetor[TAM];
    for(int i = 0; i < TAM; i++){
        cout<<"Valor para x ["<< i <<"]: ";
        cin>>vetor[i].x;
    }    
    for(int j = 0; j < TAM; j++){
        cout<<"Valor para y ["<< j <<"]: ";
        cin >> vetor[j].y;
    } 
}
void maiorValor(struct pontos vetor[], int TAM){
    int menorX = 0, menorY = 0;
    for(int i = 0; i < TAM; i++){
        if(vetor[i].x > menorX ){
            menorX = vetor[i].x ;
        }
    }
    for(int j = 0; j < TAM; j++){
        if(vetor[j].y > menorY){
            menorY = vetor[j].y;
        }
    }
    cout<< "a) Maior valor de x = "<< menorX<<endl;
    cout<< "b) Maior valor de y = "<< menorY<<endl;
}
int main(void) {
    struct pontos vetor[TAM];

    preenchaVetor(vetor, TAM);
    maiorValor(vetor, TAM);
    return 0;
}


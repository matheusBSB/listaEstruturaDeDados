/* 
 * File:   exer11_ETD.cpp
 * Author: Devs
 *
 * Created on 6 de Maio de 2018, 00:15
 * 11. Faça um programa que receba um vetor com 10 posições e peça para que o usuário escolha as
seguintes opções de ordenamento:
a) selection sort
b) insertion sort
c) bublle sort
 */

#include <iostream>
using namespace std;
const int TAM = 10;
int vetor[TAM];


void preencherVetor(int vetor[], const int TAM){
    cout<< "PREENCHA O Vetor "<<endl;
    for(int i = 0; i < TAM; i++){  
        cout<< "Posicao ["<< i <<"] = ";
        cin>>vetor[i];
    }
}
void selectioSort (int vetor[], const int TAM){
    cout<< "\n\t>>>>>>Ordenacao selectioSort<<<<<< " <<endl;
    int menor;
    for(int primeiroIndice = 0; primeiroIndice < TAM; primeiroIndice++){
        int menor = primeiroIndice;
        for(int direita = primeiroIndice+1; direita < TAM; direita++){
            if(vetor[direita] < vetor[menor]){
                menor = direita;
            }
            if(primeiroIndice != menor){
                int temp = vetor[primeiroIndice];
                vetor[primeiroIndice] = vetor[menor];
                vetor[menor] = temp;
            }
        }
    }
    for(int i = 0; i < TAM; i++){
        cout<< "Posicao ["<< i << "]: "<< vetor[i] <<endl;
    }
}
void insertionSort( int vetor[], const int TAM){
    cout<< "\n\t>>>>>>ORDENACAO INSERTIONsORT<<<<<<"<<endl;
   int atual = 0, j = 0, i = 0;
   for (i = 1; i < TAM; i++){
       atual = vetor[i];
       j = i - 1;//anterior
      
       while((j >= 0) && (atual < vetor[j])){
           vetor[j + 1] = vetor[j];
           j = j - 1;
           
       }
       vetor[j + 1] = atual;
        
    }
   for(int i = 0; i < TAM; i++){
       cout << "Posicao ["<< i << "]: "<< vetor[i]<<endl;
   }
   
           
}
void bublleSort(int vetor[], int TAM){
    cout<< "\n\t>>>>>>ORDENCAO BUBLLE SORT<<<<<<" << endl;
    for( int x = 0; x < TAM; x++){       
        for(int y = x+1; y < TAM; y++){
            if(vetor[x] > vetor[y]){
                int maior = vetor[x];
                vetor[x] = vetor[y];
                vetor[y] = maior;
            }
        }
    }
    for( int i = 0; i < TAM; i++){
        cout<< "Posicao ["<< i << "]: "<<vetor[i] << endl;
    }
    
}
void menu(int op, int cont, const int TAM, int vetor[]){
    do{
        cont++;
        if(cont < TAM)
            preencherVetor(vetor, TAM);
        cout<< "\n\t>>>>>>-MENU-<<<<<<"<<endl;
        cout <<"\tDigite Zero para Encerrar"<<endl;
        cout<< "Escolha um dos numeros  das opcoes de ordenacao!" <<endl;
        cout <<"1.a) selection sort"<<endl;
        cout<< "2.b) insertion sort"<<endl;
        cout<< "3.c) bublle sort"<<endl;
        cout<< "Opcao.: ";
        cin>> op;
        if(op < 0 || op > 3)
            cout << "Opcao INVALIDA!! " <<endl;
        switch(op) { 
            cout<<"teste case 1 "<<endl;
            case 1:
                selectioSort(vetor, TAM);
                break;
            case 2:
                insertionSort(vetor, TAM);
                break;
            case 3:
                bublleSort(vetor, TAM);
                break;
        }
    }while(op != 0);// }while(op != 'a' && op != 'A' && op != 'b' && op != 'B' && op != 'c' && op != 'C');//while(op == 'a' && op == 'A' && op == 'b' && op == 'B' && op == 'c' && op == 'C');
    cout<<"Programa Encerrado"<<endl;
}
int main(void) {
    int op = 0, cont = 0;
    
     menu(op, cont, TAM, vetor);
    
    
    return 0;
}

